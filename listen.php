<?php
$db = new SQLite3("src/hite-pro.db");
if (!$db)
    exit("Невозможно открыть базу данных!");

function query($db, $sql)
{
    $waiting = true; // Set a loop condition to test for
    while ($waiting) {
        if (!$res = $db->query($sql)) {
            usleep(250000);
        } else {
            return $res;
            $waiting = false;
        }
    }
}

$sql = "SELECT * FROM listen ORDER BY id DESC";
$res = query($db, $sql);
while ($q = $res->fetchArray(1)) echo $q['date'] . " " . $q['signal'] . "<br>";

$db->close();