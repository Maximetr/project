<?php

include_once "src/common.php";

$template = 'forgot.twig';

$data = array();

if (isset ($_SESSION['user'])) {
    header('Location: /');
    exit;
}

if (isset($_POST['email']) && $_POST['email'] && !isset($_POST['answer'])) {

    $res = query($db, "SELECT * FROM users WHERE email = '" . $_POST['email'] . "'");
    $user = $res->fetchArray(1);
    if ($user) {
        $data['quest'] = $user['question'];
        $data['email'] = $_POST['email'];
        //TODO: add email to user with recovery link
    } else {
        $data['error'] = 'Пользователь не найден';
    }
} else if (isset($_POST['answer'])) {
    $res = query($db, "SELECT * FROM users WHERE email = '" . $_POST['email'] . "'");
    $user = $res->fetchArray(1);
    if ($user) {
        $data['quest'] = $user['question'];
        $data['email'] = $_POST['email'];

        if (mb_strtolower($user['answer']) == mb_strtolower($_POST['answer'])) {
            $password_hash = password_hash($_POST['passwordrec'], PASSWORD_DEFAULT);
            query($db, "UPDATE users SET password='" . $password_hash . "' WHERE id='" . $user['id'] . "'");
            $user['password'] = $password_hash;
            $_SESSION['user'] = $user;
//        setcookie('_identity', json_encode(array('id' => $user['id'], 'auth_key' => $user['auth_key'])), time() + 2592000);
            header('Location: /');
            //TODO: add new password entering
        } else {
            $data['error'] = 'Неверный ответ на контрольный вопрос';
        }
    }
}

require_once 'src/template.php';