<?php
require 'db.php';
include 'php_scripts/hilink.php';
include 'php_scripts/crypto.php';

// Для взаимодействия с Hilink модемом для приема, отправки смс
// $router = new HuaweiHilinkApi\Router;
// $router->setAddress('192.168.0.254');
// $router->login('admin', '');
// var_dump($router->getInbox());
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<script type="text/javascript" src="java_scripts/jquery.js"></script>
<script type="text/javascript" src="java_scripts/jquery.form.js"></script>
<?php include 'java_scripts/custom.js';
if (isset ($_SESSION['logged_user'])) { ?>
    <a href="index.php">Главная</a><br>
    <a href="settings.php?cat=1">Добавление новых устройств</a><br>
    <a href="settings.php?cat=2">Категории</a><br>
    <a href="settings.php?cat=3">Сценарии</a><br>
    <a href="settings.php?cat=4">Настройки</a><br>

    <?php
    switch ($_GET["cat"]) {
        case 4:
            if (!empty($_POST['id'])) {
                $id = $_POST['id'];
                echo $_POST['mac_' . $id];
                echo "</br>";
                echo $_POST['ch_' . $id];
                echo "</br>";
                echo $_POST['ssid_' . $id];
                echo "</br>";
                echo $_POST['enc_' . $id];
                echo "</br>";
                echo $_POST['pass'];
                echo "</br>";
                $f = file("/etc/config/wireless");
                $fd = fopen("/etc/config/wireless", 'w');
                foreach ($f as $value) {
                    if (stristr($value, "channel")) {
                        $t = explode("'", $value);
                        $ch = $_POST['ch_' . $id];
                        fwrite($fd, $t[0] . "'" . $ch . "'\r\n");
                    } elseif (stristr($value, "network")) {
                        $t = explode("'", $value);
                        fwrite($fd, $t[0] . "'wwan'\r\n");
                    } elseif (stristr($value, "ssid")) {
                        $t = explode("'", $value);
                        $ssid = $_POST['ssid_' . $id];
                        fwrite($fd, $t[0] . "'" . $ssid . "'\r\n");
                    } elseif (stristr($value, "encryption")) {
                        $t = explode("'", $value);
                        $enc = $_POST['enc_' . $id];
                        fwrite($fd, $t[0] . "'" . $enc . "'\r\n");
                    } elseif (stristr($value, " mode")) {
                        $t = explode("'", $value);
                        fwrite($fd, $t[0] . "'sta'\r\n");
                    } elseif (stristr($value, "bssid")) {
                        $t = explode("'", $value);
                        $mac = $_POST['mac_' . $id];
                        fwrite($fd, $t[0] . "'" . $mac . "'\r\n");
                    } elseif (stristr($value, "key")) {
                        $t = explode("'", $value);
                        $pass = $_POST['pass'];
                        fwrite($fd, $t[0] . "'" . $pass . "'\r\n");
                    } else fwrite($fd, $value);
                }
                fclose($fd);
                system("/etc/init.d/network restart", $a);
                if ($a == 0) echo "Все ок"; else echo "Ошибка";
                sleep(15);
                exec("iwconfig wlan0", $r);
                $t = FALSE;
                foreach ($r as $value) {
                    if (stristr($value, "not-associated")) $t = TRUE;
                }
                if ($t) {
                    echo "<br>не подключено";
                    unlink("/etc/config/wireless");
                    copy("/etc/config/wireless.ap", "/etc/config/wireless");
                    system("/etc/init.d/network restart", $a);
                    if ($a == 0) echo "<br>Возвращаем настройки";
                }
            } else {
                echo '<form method="post">';
                exec("iwlist wlan0 scanning", $aps);
                $i = 0;

                foreach ($aps as $value) {
                    if (stristr($value, "Address")) {
                        $i++;
                        $mac = explode(": ", $value);
                        echo "<input type='hidden' name='mac_" . $i . "' value='" . $mac[1] . "'>";
                    }
                    if (stristr($value, "Channel:")) {
                        $ch = explode(":", $value);
                        echo "<input type='hidden' name='ch_" . $i . "' value='" . $ch[1] . "'>";
                    }
                    if (stristr($value, "essid")) {
                        $ssid = explode('"', $value);
                        echo "<input type='radio' name='id' value='" . $i . "'>";
                        echo "<input type='hidden' name='ssid_" . $i . "' value='" . $ssid[1] . "'>" . $ssid[1] . "</br>";
                    }
                    if (stristr($value, "WPA2")) {
                        echo "<input type='hidden' name='enc_" . $i . "' value='psk2'>";
                    }
                    if (stristr($value, "WPA Version")) {
                        echo "<input type='hidden' name='enc_" . $i . "' value='psk'>";
                    }
                }
                echo 'Пароль, если требуется: <input type="text" name="pass">';
                echo '<button type="submit">Сохранить</button></form>';
            }
            break;
        case 2:
            echo '<form method="post">';
            include 'cat.php';
            echo '<button type="submit">Сохранить</button></form>';
            break;
        case 1:
            echo '<form method="post">';
            include 'add.php';
            echo '</form>';
            break;
    }
}
?>
</body>
</html>