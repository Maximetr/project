<?php

/**
 * Class PostFile Class for POST files
 */
class PostFile
{
    public $file;
    public $fileName;
    public $tmpName;

    /**
     * PostFile constructor.
     * @param $name string Input attribute 'name'
     */
    public function __construct($name)
    {
        $this->file = $_FILES["$name"];
        $this->fileName = $this->file['name'];
        $this->tmpName = $this->file['tmp_name'];
    }

    public function fileSave()
    {
        //проверить формат файла
        if ($this->isDbExtension()) {
            //переименование транзитного файла
            rename('src/filesTest/old-hite-pro.db', 'src/filesTest/tmp-hite-pro.db');
            //получить старый файл бд и переименовать его в old-hite-pro.db
            rename('src/filesTest/hite-pro.db','src/filesTest/old-hite-pro.db');
            //записать новый файл с нужным именем
            rename("$this->tmpName", 'src/filesTest/hite-pro.db');
            return true;
        }

        return false;
        //если нужно откатить изменения, то метод resetChanges()
    }
    /**
     * @return bool
     * @var $fileInfo object
     */
    private function isDbExtension()
    {
        $fileInfo = new SplFileInfo($this->fileName);
        if ($fileInfo->getExtension() == 'db') {
            return true;
        }
        return false;
    }

    private function resetChanges()
    {
        rename('src/filesTest/hite-pro.db', 'src/filesTest/idn.db');
        rename('src/filesTest/old-hite-pro.db', 'src/filesTest/hite-pro.db');
        rename('src/filesTest/tmp-hite-pro.db', 'src/filesTest/old-hite-pro.db');
        unlink('src/filesTest/idn.db');
        $tmp = fopen('src/filesTest/tmp-hite-pro.db', "w");
        fclose($tmp);
        return true;
    }

}



