<?php
function getCat($db)
{
    $sql = 'SELECT * FROM categories ORDER BY sort';
    $res = $db->query($sql);

    //Создаем масив где ключ массива является ID меню
    $cat = array();
    while ($row = $res->fetchArray(1)) {
        $cat[$row['id']] = $row;
// var_dump($row);
    }
    return $cat;
}

//Функция построения дерева из массива от Tommy Lacroix
function getTree($dataset)
{
    $tree = array();

    foreach ($dataset as $id => &$node) {
        //Если нет вложений
        if (!$node['parent']) {
            $tree[$id] = &$node;
        } else {
            //Если есть потомки то перебераем массив
            $dataset[$node['parent']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

//Шаблон для вывода меню в виде дерева
function tplMenu($category)
{
    global $where;
    $menu = '<li>
		<div id="menu_' . $category['id'] . '" style="display:block"><a href="?cat=' . $category['id'] . '" title="' . $category['title'] . '">' .
        $category['title'] . '</a></div><div id="change_' . $category['id'] . '" style="display:none"><input type="text" name="change_' . $category['id'] . '" value="' . $category['title'] . '"></div>
		<a href="javascript://" onclick="$(\'div#menu_' . $category['id'] . '\').toggle();$(\'div#change_' . $category['id'] . '\').toggle();return false;"><img src="images/change.png"></a>
		<a href="?cat=' . $where . '&del=' . $category['id'] . '"><img src="images/trash.png"></a>
		<a href="?cat=' . $where . '&up=' . $category['id'] . '&sort=' . $category['sort'] . '&parent=' . $category['parent'] . '"><img src="images/up.png"></a>
		<a href="?cat=' . $where . '&down=' . $category['id'] . '&sort=' . $category['sort'] . '&parent=' . $category['parent'] . '"><img src="images/down.png"></a>';

    if (isset($category['childs'])) {
        $menu .= '<ul>' . showCat($category['childs']) . '</ul>';
    }
    if ($category['parent'] == 0) {
        $menu .= '<ul><a href="javascript://" onclick="$(\'div#add_' . $category['id'] . '\').toggle();return false;">Добавить</a></li><div id="add_' . $category['id'] . '" style="display:none"><input type="hidden" name="id_' . $category['id'] . '" value="' . $category['id'] . '"><input type="text" name="name_' . $category['id'] . '"></div></ul>';
    }
    $menu .= '</li>';

    return $menu;
}

/**
 * Рекурсивно считываем наш шаблон
 **/
function showCat($data)
{
    $string = '';
    foreach ($data as $item) {
        $string .= tplMenu($item);
    }
    return $string;
}

$id = trim(strip_tags($_POST["id"]));
$del = trim(strip_tags($_GET["del"]));
$up = trim(strip_tags($_GET["up"]));
$down = trim(strip_tags($_GET["down"]));
$sort = trim(strip_tags($_GET["sort"]));
$parent = trim(strip_tags($_GET["parent"]));
$where = trim(strip_tags($_GET["cat"]));

if ($id == '0') {
    foreach ($_POST as $key => $val) {
        if (stristr($key, 'id') == TRUE) {
            $name = trim(strip_tags($_POST["name_" . $val]));
            if (!empty($name)) {
                $sort = 10;
                $sql = 'SELECT sort FROM categories WHERE  parent = ' . $val . ' ORDER BY sort DESC LIMIT 1';
                $res = $db->query($sql);
                $row = $res->fetchArray(1);
                if ($row) {
                    $sort = $row['sort'] + 1;
                }
                $sql = "INSERT INTO categories ('title','parent','sort') VALUES ('" . $name . "', '" . $val . "','" . $sort . "' )";
                $db->exec($sql);//добавим записи в таблицу
            }
        }
        if (stristr($key, 'change') == TRUE) {
            $n = str_replace("change_", "", $key);
            $sql = "UPDATE categories SET title='" . $val . "' WHERE id=" . $n;
            $db->exec($sql);
        }
    }
}
if (!empty($del)) {
    $sql = "DELETE FROM categories WHERE id = " . $del . " OR parent = " . $del;
    $db->exec($sql);
}
if (!empty($up)) {
    $sql = 'SELECT sort FROM categories WHERE sort < ' . $sort . ' AND parent = ' . $parent . ' ORDER BY sort DESC LIMIT 1';
    $res = $db->query($sql);
    $row = $res->fetchArray(1);
    $sort = $row['sort'] - 1;
    if ($row) {
        $sql = 'SELECT id FROM categories WHERE sort = ' . $sort . ' AND parent = ' . $parent . ' ORDER BY sort DESC LIMIT 1';
        $res = $db->query($sql);
        $s = $res->fetchArray(1);
        if ($s) {
            $sql = "UPDATE categories SET sort = sort-1 WHERE parent = " . $parent . " AND sort < " . $row['sort'] . " AND id != " . $up;
            $db->exec($sql);
        }
        $sql = "UPDATE categories SET sort = " . $sort . " WHERE id = " . $up;
        $db->exec($sql);
    }
}
if (!empty($down)) {
    $sql = 'SELECT sort FROM categories WHERE sort > ' . $sort . ' AND parent = ' . $parent . ' ORDER BY sort LIMIT 1';
    $res = $db->query($sql);
    $row = $res->fetchArray(1);
    $sort = $row['sort'] + 1;
    if ($row) {
        $sql = 'SELECT id FROM categories WHERE sort = ' . $sort . ' AND parent = ' . $parent . ' ORDER BY sort  LIMIT 1';
        $res = $db->query($sql);
        $s = $res->fetchArray(1);
        if ($s) {
            $sql = "UPDATE categories SET sort = sort+1 WHERE parent = " . $parent . " AND sort > " . $row['sort'] . " AND id != " . $down;
            $db->exec($sql);
        }
        $sql = "UPDATE categories SET sort = " . $sort . " WHERE id = " . $down;
        $db->exec($sql);
    }
}


//Получаем подготовленный массив с данными
$cat = getCategories($db);

//Создаем древовидное меню
$tree = getTree($cat);

//Получаем HTML разметку
$cat_menu = showCat($tree);

//Выводим на экран  onclick="toggle(add);return false;"
echo '<ul>' . $cat_menu . '<li><a href="javascript://" onclick="$(\'div#add\').toggle();return false;">Добавить</a></li><div id="add" style="display:none"><input type="hidden" name="id" value="0"><input type="text" name="name_0"></div></ul>';

$db->close(); //закроем базу

?>

