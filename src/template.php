<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twigTest = new Twig_SimpleTest('ondisk', function ($file) {
    return file_exists($file);
});
$twig = new Twig_Environment($loader, array('cache' => false/*'cache'*/));
$twig->addTest($twigTest);

if (!isset($back_url)) {
    if (isset($_SERVER['HTTP_REFERER'])) {
        if (substr($_SERVER['HTTP_REFERER'], strlen($_SERVER['HTTP_REFERER']) - 1) == '/')
            $back_url = '';
        else
            $back_url = basename($_SERVER['HTTP_REFERER']);
    } else {
        $back_url = '';
    }
}

echo $twig->render($template, array('data' => $data, 'back_url' => $back_url));

$db->close();