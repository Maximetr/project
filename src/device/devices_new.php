<?php
/**
 * Created by PhpStorm.
 * User: work-user
 * Date: 07.11.18
 * Time: 16:56
 */

$devices_new = array(
    array(
        'title' => 'Relay 4',
        'model' => 'relay4',
        'serial_onoff' => '87.157.206.138.60.4.13',
        'counter' => '12345',
        'type' => 'switch'
    ),
    array(
        'title' => 'Temp 1',
        'model' => 'temp1',
        'serial_onoff' => '87.157.206.138.60.4.13',
        'counter' => '12345',
        'type' => 'temperature'
    ),
    array(
        'title' => 'Humidity 1',
        'model' => 'humidity1',
        'serial_onoff' => '87.157.206.138.60.4.13',
        'counter' => '12345',
        'type' => 'humidity'
    ),
    array(
        'title' => 'Illumination 1',
        'model' => 'illumination1',
        'serial_onoff' => '87.157.206.138.60.4.13',
        'counter' => '12345',
        'type' => 'illumination'
    ),
);