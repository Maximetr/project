<?php
function uart_sent($string)
{
    $code_string = null;
    system('./etc/code.out <<< $string', $code_string);

    $fd = dio_open('/dev/ttyS1', O_RDWR | O_NOCTTY | O_NONBLOCK);
    dio_write($fd, $code_string);
    dio_close($fd);
}

function uart_read()
{
    $result = "";
    $fd = dio_open('/dev/ttyS1', O_RDWR | O_NOCTTY | O_NONBLOCK);
    $result = dio_read($fd);
    dio_close($fd);
    $decode_result = null;
    system('./etc/decode.out <<< $result', $decode_result);
    return $decode_result;
}

?>