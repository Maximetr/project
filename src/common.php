<?php
session_start();
$db = new SQLite3("src/hite-pro.db");
if (!$db)
    exit("Невозможно открыть базу данных!");

function query ($db,$sql)
{
$waiting = true; // Set a loop condition to test for
while($waiting) {
if (!$res = $db->query($sql))
	{
            return false; 	}
else {
	return $res;
        $waiting = false;
		} 
	}
}


if ($_SERVER['SCRIPT_NAME'] != '/signin.php' && $_SERVER['SCRIPT_NAME'] != '/signup.php' && $_SERVER['SCRIPT_NAME'] != '/forgot.php') {
    if (!isset ($_SESSION['user'])) {
            header('Location: /signin.php');
            exit;
    } 
}

function mb_basename($param, $suffix=null){
    $charset = 'utf-8';
    if ( $suffix ) {
        $tmpstr = ltrim(mb_substr($param, mb_strrpos($param, DIRECTORY_SEPARATOR, null, $charset), null, $charset), DIRECTORY_SEPARATOR);
        if ( (mb_strpos($param, $suffix, null, $charset)+mb_strlen($suffix, $charset) ) == mb_strlen($param, $charset) ) {
            return str_ireplace( $suffix, '', $tmpstr);
        } else {
            return ltrim(mb_substr($param, mb_strrpos($param, DIRECTORY_SEPARATOR, null, $charset), null, $charset), DIRECTORY_SEPARATOR);
        }
    } else {
        return ltrim(mb_substr($param, mb_strrpos($param, DIRECTORY_SEPARATOR, null, $charset), null, $charset), DIRECTORY_SEPARATOR);
    }
}

function getCategory($db, $id) {
    $category = array();

    if ($id) {
        $sqlCategories = "SELECT * FROM categories WHERE id = '" . $id . "'";
        $resCat = query($db,$sqlCategories);

        if($cat = $resCat->fetchArray(1))
            $category = $cat;
    }

    return $category;
}

function getDevice($db, $id) {
    $device = array();

    if ($id) {
        $sqlDevices = "SELECT * FROM devices WHERE id = '" . $id . "'";
        $resDev = query($db,$sqlDevices);

        if($dev = $resDev->fetchArray(1))
            $device = $dev;
    }

    return $device;
}

function getCategories($db, $with_device) {
    $sqlCategories = "SELECT * FROM categories ORDER BY categories.sort";
    $resCat = query($db,$sqlCategories);

    $categories = array();
    while($category = $resCat->fetchArray(1)) {
        if ($with_device) {
            $category['devices'] = array();
            $sqlDevices = "SELECT * FROM devices WHERE category = '".$category['id']."'";
            $resDev = query($db,$sqlDevices);
            while($device = $resDev->fetchArray(1))
                $category['devices'][] = $device;
        }

        $categories[$category['id']] = $category;
    }
    return $categories;
}

function getDevices($db) {
    $sqlDevices = "SELECT * FROM devices ORDER BY devices.category, devices.sort";
    $resDev = query($db,$sqlDevices);

    $devices = array();
    while($device = $resDev->fetchArray(1)) {
        $devices[$device['id']] = $device;
    }
    return $devices;
}

function getIcons() {
    $icons = array();
    foreach (glob("templates/icons/device/*.svg") as $filename) {
        $icons[] = str_replace('.svg', '', mb_basename($filename));
    }
    return $icons;
}

function getUserByEmail($db, $email)
{
    $sql = "SELECT * FROM users WHERE email = '" . $email . "'";

    $result = query($db,$sql);
    if ($result) {
        $user = array();
        return $user = $result->fetchArray(1);
    } else {
        return false;
    }
}


