"use strict";
$(document).ready(function () {
    $(".raise_priority_category").click(function () {
        var e = $(this).parent(".room"), i = e.prevAll(".room").first(), t = e.data("id"), a = i.data("id");
        "undefined" != typeof t && "undefined" != typeof a && $.post("index-ajax.php", {
            dataType: "json",
            type: "category",
            id_up: t,
            id_down: a
        }).done(function (a) {
            "ok" === a && (e.insertBefore(i), $.each($('.line.object[data-category="' + t + '"]').toArray().reverse(), function (i, t) {
                $(t).insertAfter(e)
            }), e.is(".form-categories .line.room:first") && (e.find(".raise_priority_category").addClass("invisible"), e.find(".raise_priority_category").find("svg > path").css("visibility", "hidden")), i.find(".raise_priority_category").removeClass("invisible"), i.find(".raise_priority_category").find("svg > path").css("visibility", "visible"))
        })
    }), $(".raise_priority_device").click(function () {
        var e = $(this).parent(".object"), i = e.prevAll(".object").first(), t = e.data("id"), a = i.data("id"),
            n = e.data("category");
        "undefined" != typeof t && "undefined" != typeof a && $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            id_up: t,
            id_down: a
        }).done(function (t) {
            "ok" === t && (e.insertBefore(i), e.is('.form-categories .line.object[data-category="' + n + '"]:first') && (e.find(".raise_priority_device").addClass("invisible"), e.find(".raise_priority_device").find("svg > path").css("visibility", "hidden")), i.find(".raise_priority_device").removeClass("invisible"), i.find(".raise_priority_device").find("svg > path").css("visibility", "visible"))
        })
    })
}), $(document).ready(function () {
    $(".btn-test").click(function (e) {
        if (e.preventDefault(), e.stopPropagation(), !$(this).hasClass("disabled")) {
            var i = $(this).parents(".nav").data("func"), t = $(this).parents(".nav").data("model"),
                a = $("#serial_" + t + "_" + i + "_temp").val();
            "undefined" != typeof a && a && $.post("index-ajax.php", {
                dataType: "json",
                type: "device",
                action: "repeat",
                serial: a
            }).done(function (e) {
            })
        }
    }), $(".btn-signal").click(function (e) {
        e.preventDefault(), e.stopPropagation();
        var i = $(this).parents(".collapse").data("func"), t = $(this).parents(".collapse").data("model"),
            a = $("#serial_" + t + "_" + i + "_temp").val();
        "undefined" != typeof a && a ? $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            action: "repeat",
            serial: a
        }).done(function (e) {
        }) : ($(this).siblings(".btn-connect").removeClass("disabled"), $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            action: "send"
        }).done(function (e) {
            "undefined" !== e && e && $("#serial_" + t + "_" + i + "_temp").val(e)
        }))
    }), $(".btn-connect").click(function (e) {
        if (e.preventDefault(), e.stopPropagation(), !$(this).hasClass("disabled")) {
            var i = $(this).parents(".collapse").data("func"), t = $(this).parents(".collapse").data("model");
            $(this).hasClass("active") ? $("#serial_" + t + "_" + i).val("") : $("#serial_" + t + "_" + i).val($("#serial_" + t + "_" + i + "_temp").val()), $(this).toggleClass("active");
            var a = $('.nav[data-func="' + i + '"][data-model="' + t + '"]');
            a.find(".btn-test").toggleClass("disabled"), a.find(".icon-check").toggleClass("d-none"), $('.collapse[data-func="' + i + '"]').collapse("hide")
        }
    }), $("#selectModelDeviceOld").change(function (e) {
        $(".section.settings").addClass("d-none");
        var i = $(this).val();
        i && ($('input[name="model"]').val(i), $('div[id="settings-' + i + '"').removeClass("d-none"))
    }), $("#buttonScanDevice").click(function (e) {
        e.preventDefault(), e.stopPropagation(), $(this).addClass("disabled"), $("#search-in-progress").removeClass("d-none"), $("#search-repeat").addClass("d-none"), $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            action: "scan"
        }).done(function (e) {
            "undefined" != typeof e.device ? "undefined" != typeof e.device.model && "undefined" != typeof e.device.serial_onoff && "undefined" != typeof e.device.counter && ($("#selectModelDeviceNew").val(e.device.model), $("#selectModelDevice_new").val(e.device.model), $("#serial_onoff_new").val(e.device.serial_onoff).attr("name", "serial_" + e.device.model + "_onoff"), $("#inputCounterDevice").val(e.device.counter), $(".section.d-none").removeClass("d-none"), $(".btn-save.d-none").removeClass("d-none"), $(".section.device-search").addClass("d-none")) : ($("#buttonScanDevice").removeClass("disabled"), $("#search-repeat").removeClass("d-none"), $("#search-in-progress").addClass("d-none"))
        })
    })
}), $("#buttonScanNetwork").click(function (e) {
    e.preventDefault(), e.stopPropagation(), $(this).addClass("disabled"), $("#search-in-progress").removeClass("d-none"), $("#search-repeat").addClass("d-none"), $.post("testajax.php", {
        dataType: "json",
        type: "network",
        action: "scan"
    }).done(function (data) {
        var networks = JSON.parse(data);

        if (networks !== false) {
            $.each(networks, function (networkId, properties) {

                $('<li>', {id:'li'+networkId}).appendTo('#networks');
                $('<input>', {id:'input'+networkId, type:'radio', name:'network', value:networkId}).appendTo('#li'+networkId);
                $('<label>', {for:'input'+networkId, text:properties["ssid"]}).appendTo('#li'+networkId);

                $('<input>', {type:'hidden', name:'macAdress_'+networkId, value:properties['macAdress']}).appendTo('#li'+networkId);
                $('<input>', {type:'hidden', name:'channel_'+networkId, value:properties['channel']}).appendTo('#li'+networkId);
                $('<input>', {type:'hidden', name:'ssid_'+networkId, value:properties['ssid']}).appendTo('#li'+networkId);
                $('<input>', {type:'hidden', name:'encryption_'+networkId, value:properties['encryption']}).appendTo('#li'+networkId);
            });
            $(".section.d-none").removeClass("d-none"), $(".section.device-search").addClass("d-none")

            var networksReady = true;
        } else {
            var networksReady = false;
            $("#search-repeat").removeClass("d-none"), $("#search-in-progress").addClass("d-none"), $("#buttonScanNetwork").removeClass("disabled")
        }


        if (networksReady){
            $("#tryToConnect").click(function () {
                $(this).addClass("disabled"), $("#connect-in-progress").removeClass("d-none"), $("#connect-section").removeClass("d-none"), $(".section.networks-list").addClass("d-none");
                var networkId = $('input[name="network"]:checked').val();
                var networkData = [];
                $('input[type=hidden]').each(function () {
                    if ($(this).attr("name") === "macAdress_"+networkId){
                        networkData["macAdress"] = $(this).val();
                    }
                    if ($(this).attr("name") === "channel_"+networkId) {
                        networkData["channel"] = $(this).val();
                    }
                    if ($(this).attr("name") === "ssid_"+networkId) {
                        networkData["ssid"] = $(this).val();
                    }
                    if ($(this).attr("name") === "encryption_"+networkId) {
                        networkData["encryption"] = $(this).val();
                    }
                });
                networkData['password'] = $('input[name=password]').val();
                networkData = $.extend({}, networkData);

                $.post('testajax.php', {
                    dataType: "json",
                    type: "connect",
                    action: "tryConnect",
                    data: networkData
                }).done(function (e) {
                    try {
                        var result = JSON.parse(e);
                    } catch (e) {
                        $(".btn.btn-danger").removeClass("d-none"), $("#connect-in-progress").addClass("d-none")
                    }

                    if (typeof result.success !== "undefined") {
                        $(".btn.btn-success").removeClass("d-none"), $("#connect-in-progress").addClass("d-none")
                    } else {
                        $(".btn.btn-danger").removeClass("d-none"), $("#connect-in-progress").addClass("d-none")
                    }
                })
            })
        }
    })
}), $(document).ready(function () {
    $(".slider-devices").slick({
        infinite: !0,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: !0,
        arrows: !1,
        asNavFor: ".slider-categories",
        fade: !1,
        adaptiveHeight: !1
    }), $(".slider-categories").slick({
        infinite: !0,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: !0,
        arrows: !0,
        asNavFor: ".slider-devices"
    }), $(".btn-switch-new").click(function (e) {
        e.preventDefault(), e.stopPropagation();
        var i = $(this).data("id"), t = $(this).data("protocol");
        "undefined" != typeof i && i && "undefined" != typeof t && "new" == t && $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            id_switch_new: i
        }).done(function (e) {
            "undefined" != typeof e.lastinfo && ("1" == e.lastinfo ? $('.btn-switch-new[data-id="' + i + '"]').addClass("active").find(".status").html("ON") : $('.btn-switch-new[data-id="' + i + '"]').removeClass("active").find(".status").html("OFF"))
        }).always(function () {
            $(".action-loader").remove()
        })
    }), $(".btn-switch-old").click(function (e) {
        e.preventDefault(), e.stopPropagation();
        var i = $(this).data("id"), t = $(this).data("protocol");
        "undefined" != typeof i && i && "undefined" != typeof t && "old" == t && $.post("index-ajax.php", {
            dataType: "json",
            type: "device",
            id_switch_old: i
        }).always(function () {
            $(".action-loader").remove()
        })
    })
}), $(document).ready(function () {
    $(".menu-item .menu, .menu-item .close").on("click", function () {
        var e = $("input[type=checkbox]");
        e.prop("checked", !e.prop("checked")), $("html, body").toggleClass("noscroll");
        var i = e.is(":checked") ? "Меню настроек" : "Дом";
        $("a.index-link").text(i), $(".menu-item.filter").toggleClass("invisible"), $(".menu-item .menu svg").toggleClass("d-none"), $(".menu-item .close svg").toggleClass("d-block")
    }), $(".wrap").click(function () {
        $(".menu-item .close").click()
    }), $(".wrap > .submenu, div.top-bar").click(function (e) {
        e.stopPropagation()
    }), $(".link-with-loader, .btn-save, .btn-confirm-delete").click(function (e) {
        var i = $('<img src="/assets/img/load.gif" class="action-loader" alt="" width="60" height="60">');
        $(".main-content").append(i)
    })
}), $(document).ready(function () {
    $(".errors").click(function () {
        $(this).addClass("d-none");
    })
});