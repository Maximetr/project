<?php

include_once "src/common.php";

$template = 'signin.twig';

$data = array();

if (isset ($_SESSION['user'])) {
    header("Location: /");
    exit;
}

if ((isset($_POST['email']) && isset($_POST['password']) && $_POST['email'] && $_POST['password']) || (isset($_GET['email']) && isset($_GET['password']) && $_GET['email'] && $_GET['password'])) {
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
        $pass = $_POST['password'];
    } else {
        $email = $_GET['email'];
        $pass = $_GET['password'];
    }
    $res = query($db, "SELECT * FROM users WHERE email = '" . $email . "'");
    $user = $res->fetchArray(1);
    if ($user) {
        if (password_verify($pass, $user['password'])) {
            $_SESSION['user'] = $user;
//            if (isset($_POST['remember-me']) && $_POST['remember-me']) {
//                setcookie('_identity', json_encode(array('id' => $user['id'], 'auth_key' => $user['auth_key'])), time() + 2592000);
//            }
            header('Location: /');
            exit;
        } else {
            $data['error'] = 'Неверный пароль';
        }

    } else {
        $data['error'] = 'Пользователь не найден';
    }
}
$res = query($db, "SELECT * FROM users");
$reg = $res->fetchArray(1);
if (!$reg) $data['reg'] = true;

require_once 'src/template.php';

