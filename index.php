<?php

include_once "src/common.php";
include_once "src/device/devices_new.php";

$template = 'index.twig';
$data = array();


$favourites = false;
$devices_tmp = array(
    'switch' => array(),
    'temperature' => array(),
    'humidity' => array(),
    'illumination' => array(),
);
$devices = getDevices($db);
$data['categories'] = array_merge(
    array('home' =>
        array('id' => 'home', 'title' => 'Дом')
    ),
    getCategories($db, false)
);

if (!empty($devices)) {
    foreach ($devices as &$device) {
        $type = 'switch';
        foreach ($devices_new as $device_new)
            if ($device_new['model'] == $device['model']) {
                $type = $device_new['type'];
                break;
            }
        $device['type'] = $type;
        $devices_tmp[$type][] = $device;
        if ($device['main_screen']) {
            $favourites = true;
        }
    }
}

if ($favourites)
    $data['categories']['favourites'] = array('id' => 'favourites', 'title' => 'Избранное');

$data['devices'] = array(
    'switch' => $devices_tmp['switch'],
    'sensor' => array_merge($devices_tmp['temperature'], $devices_tmp['humidity'], $devices_tmp['illumination'])
);

if ($_SESSION['user']['main']) {
    foreach ($data['categories'] as $index => $category) {
        if ($category['id'] == $_SESSION['user']['main']) {
            $main_screen = $category;
            unset($data['categories'][$index]);
            $data['categories'] = array_merge(
                array($_SESSION['user']['main'] => $main_screen),
                $data['categories']
            );
            break;
        }
    }
}

require_once 'src/template.php';
