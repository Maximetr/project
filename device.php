<?php

include_once "src/common.php";
include_once "src/device/devices_old.php";
include_once "src/device/devices_new.php";

$template = 'device.twig';

$data['device'] = getDevice($db, isset($_GET['id']) && $_GET['id'] ? $_GET['id'] : '');
$data['categories'] = getCategories($db, false);
$data['icons'] = getIcons();
$data['devices'] = array(
    'old' => $devices_old,
    'new' => $devices_new,
);

if (!empty($data['device']) && isset($data['device']['model'])) {
    foreach (array_merge($devices_new, $devices_old) as $device_tmp)
        if ($device_tmp['model'] == $data['device']['model']) {
            $data['device']['title'] = $device_tmp['title'];
            if (isset($device_tmp['modes']))
                $data['device']['modes'] = $device_tmp['modes'];
            break;
        }
}


require_once 'src/template.php';





