<?php
include_once ('src/common.php');
include_once ('models/Settings.php');

if (isset($_POST['action']) && $_POST['action'] == 'scan') {
    exec("iwlist wlan0 scanning", $networks); //поиск сетей

    if ($networks) {
        $data = array();

        $i = 0;
        foreach ($networks as $value) {
            if (stristr($value, "Address")) {
                $i++;
                $mac = explode(": ", $value);
                $data[$i]['macAdress'] = $mac[1];
            }
            if (stristr($value, "Channel:")) {
                $ch = explode(":", $value);
                $data[$i]['channel'] = $ch[1];
            }
            if (stristr($value, "essid")) {
                $ssid = explode('"', $value);
                $data[$i]['ssid'] = $ssid[1];
            }
            if (stristr($value, "WPA2")) {
                $data[$i]['encryption'] = 'psk2';
            }
            if (stristr($value, "WPA Version")) {
                $data[$i]['encryption'] = 'psk';
            }
        }
    } else {
        $data = false;
    }

    echo json_encode($data);
}
if (isset($_POST['action']) && $_POST['action'] == 'tryConnect') {
    $result = array();
    $networkData = $_POST['data'];
    $file = file("/etc/config/wireless");

    $fileHandler = fopen("/etc/config/wireless", 'w');

    foreach ($file as $row) {
        if (stristr($row, "channel")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'" . $networkData['channel'] . "'\r\n");
        } elseif (stristr($row, "network")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'wwan'\r\n");
        } elseif (stristr($row, " ssid")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'" . $networkData['ssid'] . "'\r\n");
        } elseif (stristr($row, "encryption")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'" . $networkData['encryption'] . "'\r\n");
        } elseif (stristr($row, " mode")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'sta'\r\n");
        } elseif (stristr($row, "bssid")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'" . $networkData['macAdress'] . "'\r\n");
        } elseif (stristr($row, "key")) {
            $t = explode("'", $row);
            fwrite($fileHandler, $t[0] . "'" . $networkData['password'] . "'\r\n");
        } else fwrite($fileHandler, $row);
    }
    fclose($fileHandler);

    system("/etc/init.d/network restart", $a);  //перезагрузка сетевого интерфейса


    if ($a == 0) {
//        sleep(15);
        exec("iwconfig wlan0", $r); //проверка

        $t = FALSE;

        foreach ($r as $value) {
            if (stristr($value, "not-associated")) {
                $t = TRUE;
            }
        }

        if ($t) {
            unlink("config/wireless");
            copy("/etc/config/wireless.ap", "/etc/config/wireless");
            system("/etc/init.d/network restart", $a);

            $result['errors'] = 'Ошибка при подключении';
        } elseif (!$t) {
            if (Settings::saveWiFiData($db, $networkData)) {
                $result['success'] = 'Connected';
            } else {
                $result['errors'] = 'Ошибка при подключении';
            }
        }
    }


    echo json_encode($result);
}

