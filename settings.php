<?php

include_once "src/common.php";
include_once "src/classes/PostFile.php";
include_once "models/Settings.php";

$template = 'settings.twig';
$back_url = '/';


//загрузка текущих настроек
if ($settings = Settings::getSettings($db)) {
    $data['settings'] = $settings;
}   else {
    $data['errors'][] = 'Не удалось загрузить настройки';
}
//

//главный экран
if (isset($_POST['main']) && $_POST['main']) {
    if ($db->exec("UPDATE users SET main='".$_POST['main']."' WHERE id='".$_SESSION['user']['id']."'")) {
        $_SESSION['user']['main'] = $_POST['main'];
    } else {
        $data['errors'][] = 'Ошибка смены главного экрана';
    }
}
//

//загрузка файла бд
if (isset($_FILES['dbFile']['tmp_name']) && is_uploaded_file($_FILES['dbFile']['tmp_name'])) {
    $postFile = new PostFile('dbFile');
    if (!$postFile->fileSave()) {
        $data['errors'][] = 'Неверное расширение файла';
    }   else {
        $upload = true;
        $data['success'] = 'Настройки применены успешно';
    }
}
//

//ставим van
if (isset($_POST['portMode']) && $_POST['portMode'] == 'van' && $settings['network'] == 'lan') {
    if (Settings::setNetworkMode($db, $_POST['portMode'])) {
        //system van
        system("uci del network.lan.ifname");
        system("uci set network.WAN.ifname='eth0'");
        system("uci commit");
        //
        $data['success'] = 'Настройки применены успешно';
    }   else {
        $data['errors'][] = 'Ошибка при изменении режима порта';
    }
}
//

//ставим lan
if (isset($_POST['portMode']) && $_POST['portMode'] == 'lan' && $settings['network'] == 'van') {
    if (Settings::setNetworkMode($db, $_POST['portMode'])) {
        //system lan
        system("uci del network.WAN.ifname");
        system("uci set network.lan.ifname='eth0'");
        system("uci commit");
        //
        $data['success'] = 'Настройки применены успешно';
    }   else {
        $data['errors'][] = 'Ошибка при изменении режима порта';
    }
}
//

//загрузка обновленных настроек
if (isset($_POST['portMode']) || (isset($upload) && $upload == true)) {
    if ($settings = Settings::getSettings($db)) {
        $data['settings'] = $settings;
    }   else {
        $data['errors'][] = 'Не удалось загрузить настройки';
    }
}
//


$data['user'] = $_SESSION['user'];
$data['categories'] = array(
    'home' => array('id' => 'home', 'title' => 'Дом'),
    'favourites' => array('id' => 'favourites', 'title' => 'Избранное'),
);

foreach (getCategories($db, false) as $category)
    $data['categories'][$category['id']] = array('id' => $category['id'], 'title' => $category['title']);

require_once 'src/template.php';





