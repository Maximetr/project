<?php

include_once "src/common.php";

$template = 'categories.twig';
$back_url = '/';

if (isset($_POST['type']) && $_POST['type']) {
    if ($_POST['type'] == 'category') {
        if (isset($_POST['id']) && $_POST['id']) {
            if (isset($_POST['delete']) && $_POST['delete']) {
                query($db, "DELETE FROM categories WHERE id='" . $_POST['id'] . "'");
            } else {
                query($db, "UPDATE categories SET title='" . $_POST['name'] . "' WHERE id='" . $_POST['id'] . "'");
            }
        } else {
            $sqlMaxSort = "SELECT MAX(sort) AS max_sort FROM categories";
            $resMaxSort = query($db, $sqlMaxSort);
            $max_sort = 1;
            if ($sort = $resMaxSort->fetchArray(1)) {
                $max_sort = $sort['max_sort'] + 1;
            }
            query($db, "INSERT INTO categories ('title', 'parent', 'sort') VALUES ('" . $_POST['name'] . "', 0, '" . $max_sort . "' )");
        }
    } else if ($_POST['type'] == 'device') {
        if (isset($_POST['id']) && $_POST['id']) {
            if (isset($_POST['delete']) && $_POST['delete']) {
                query($db, "DELETE FROM devices WHERE id='" . $_POST['id'] . "'");
            } else {
                $model = $_POST['model'];
                $serial_onoff = (isset($_POST['serial_' . $model . '_onoff']) ? $_POST['serial_' . $model . '_onoff'] : false);
                $serial_on = (isset($_POST['serial_' . $model . '_on']) ? $_POST['serial_' . $model . '_on'] : false);
                $serial_off = (isset($_POST['serial_' . $model . '_off']) ? $_POST['serial_' . $model . '_off'] : false);

                $main_screen = (isset($_POST['main_screen']) ? '1' : '0');
                $device = array();
                $sqlDevices = "SELECT * FROM devices WHERE id = '" . $_POST['id'] . "'";
                $resDev = query($db, $sqlDevices);

                if ($dev = $resDev->fetchArray(1))
                    $device = $dev;

                if ($device['category'] != $_POST['category']) {
                    $sqlMaxSort = "SELECT MAX(sort) AS max_sort FROM devices";
                    $resMaxSort = query($db, $sqlMaxSort);
                    if ($sort = $resMaxSort->fetchArray(1)) {
                        $max_sort = $sort['max_sort'] + 1;
                    }
                }

                if ($device['main_screen'] != $main_screen && $main_screen) {
                    $sqlMaxSort = "SELECT MAX(sort_main) AS max_sort_main FROM devices";
                    $resMaxSort = query($db, $sqlMaxSort);
                    if ($sort = $resMaxSort->fetchArray(1)) {
                        $max_sort_main = $sort['max_sort_main'] + 1;
                    }
                }

                $query = "UPDATE devices SET name='" . $_POST['name'] . "', category='" . $_POST['category'] . "', icon='" . $_POST['icon'] . "'," .
                    ($serial_onoff !== false ? " serial_onoff='" . $serial_onoff . "'," : '') .
                    ($serial_on !== false ? " serial_on='" . $serial_on . "'," : '') .
                    ($serial_off !== false ? " serial_off='" . $serial_off . "'," : '') .
                    (isset($max_sort) ? " sort='" . $max_sort . "'," : '') .
                    (isset($max_sort_main) ? " sort_main='" . $max_sort_main . "'," : '') .
                    " main_screen='" . (isset($_POST['main_screen']) ? '1' : '0') . "' WHERE id='" . $_POST['id'] . "'";
                query($db, $query);
            }
        } else {
            $model = $_POST['model'];
            $serial_onoff = (isset($_POST['serial_' . $model . '_onoff']) ? $_POST['serial_' . $model . '_onoff'] : '');
            $serial_on = (isset($_POST['serial_' . $model . '_on']) ? $_POST['serial_' . $model . '_on'] : '');
            $serial_off = (isset($_POST['serial_' . $model . '_off']) ? $_POST['serial_' . $model . '_off'] : '');
            $main_screen = (isset($_POST['main_screen']) ? '1' : '0');
            $counter = (isset($_POST['counter']) ? $_POST['counter'] : '');

            $sqlMaxSort = "SELECT MAX(sort) AS max_sort, MAX(sort_main) AS max_sort_main FROM devices";
            $resMaxSort = query($db, $sqlMaxSort);
            $max_sort = 1;
            $max_sort_main = 1;
            if ($sort = $resMaxSort->fetchArray(1)) {
                $max_sort = $sort['max_sort'] + 1;
                $max_sort_main = $sort['max_sort_main'] + 1;
            }

            $query = "INSERT INTO devices ('date', 'name', 'category', 'icon', 'protocol', 'model', 'serial_onoff', 'serial_on', 'serial_off', 'main_screen', 'counter', 'sort', 'sort_main')" .
                " VALUES ('" . time() . "', '" . $_POST['name'] . "', '" . $_POST['category'] . "', '" . $_POST['icon'] . "', '" . $_POST['protocol'] . "', '" . $_POST['model'] . "', '$serial_onoff', '$serial_on', '$serial_off', '$main_screen', '$counter', '$max_sort', '$max_sort_main')";
            query($db, $query);
        }
    } else if ($_POST['type'] == 'favourites') {
        $sqlDevices = "SELECT id FROM devices";
        $resDev = query($db, $sqlDevices);

        $devices = array();
        while ($device = $resDev->fetchArray(1)) {
            $main_screen = (isset($_POST['main_screen'][$device['id']]) ? '1' : '0');
            query($db, "UPDATE devices SET main_screen='$main_screen' WHERE id='" . $device['id'] . "'");
        }
    }
} else if (isset($_POST['delete']) && $_POST['delete']) {
    $delete = @explode('-', $_POST['delete']);
    if ($delete[0] == 'category') {
        if (ctype_digit($delete['1'])) {
            //$db->exec("DELETE FROM devices WHERE category='" . $delete['1'] . "'");
            query($db, "UPDATE devices SET category='' WHERE category='" . $delete['1'] . "'");
            query($db, "DELETE FROM categories WHERE id='" . $delete['1'] . "'");
        }

    } elseif ($delete[0] == 'device') {
        if (ctype_digit($delete['1'])) {
            query($db, "DELETE FROM devices WHERE id='" . $delete['1'] . "'");
        }
    }
}

$data['categories'] = getCategories($db, false);
$data['devices'] = getDevices($db);

$no_category = false;
foreach ($data['devices'] as $device) {
    if (!$device['category']) {
        $no_category = true;
    }
}
//$data['main_screen'] = $main_screen;
$data['no_category'] = $no_category;

require_once 'src/template.php';





