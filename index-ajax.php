<?php

include_once "src/common.php";

$data = array();

//if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
if (isset($_POST['type'])) {
    if ($_POST['type'] == 'category') {
        if (isset($_POST['id_up']) && $_POST['id_up'] && isset($_POST['id_down']) && $_POST['id_down']) {
            $sqlCategories = "SELECT id, sort FROM categories WHERE id = '" . $_POST['id_up'] . "' OR id = '" . $_POST['id_down'] . "'";
            $resCat = $db->query($sqlCategories);

            $categories = array();
            while ($category = $resCat->fetchArray(1)) {
                $categories[$category['id']] = $category['sort'];
            }
            if (isset($categories[$_POST['id_up']]) && isset($categories[$_POST['id_up']])) {
                query($db, "UPDATE categories SET sort='" . $categories[$_POST['id_down']] . "' WHERE id='" . $_POST['id_up'] . "'");
                query($db, "UPDATE categories SET sort='" . $categories[$_POST['id_up']] . "' WHERE id='" . $_POST['id_down'] . "'");
                $data = 'ok';
            }
        }
    } else if ($_POST['type'] == 'device') {
        if (isset($_POST['id_up']) && $_POST['id_up'] && isset($_POST['id_down']) && $_POST['id_down']) {
            $sqlDevices = "SELECT id, sort FROM devices WHERE id = '" . $_POST['id_up'] . "' OR id = '" . $_POST['id_down'] . "'";
            $resDev = $db->query($sqlDevices);

            $devices = array();
            while ($device = $resDev->fetchArray(1)) {
                $devices[$device['id']] = $device['sort'];
            }
            if (isset($devices[$_POST['id_up']]) && isset($devices[$_POST['id_up']])) {
                query($db, "UPDATE devices SET sort='" . $devices[$_POST['id_down']] . "' WHERE id='" . $_POST['id_up'] . "'");
                query($db, "UPDATE devices SET sort='" . $devices[$_POST['id_up']] . "' WHERE id='" . $_POST['id_down'] . "'");
                $data = 'ok';
            }
            $data = 'ok';
        } else if (isset($_POST['action'])) {
            if ($_POST['action'] == 'scan') {
                include_once 'src/device/scan.php';
            } else {
                include_once 'src/device/send.php';
                if (isset($serial) && $serial) {
                    $data = $serial;
                }
            }
        } else if (isset($_POST['id_switch_new'])) {
            $sqlDevices = "SELECT lastinfo FROM devices WHERE id = '" . $_POST['id_switch_new'] . "'";
            $resDev = $db->query($sqlDevices);

            if ($device = $resDev->fetchArray(1)) {
                $update_info = ($device['lastinfo'] ? '0' : '1');
                query($db, "UPDATE devices SET lastinfo='$update_info' WHERE id='" . $_POST['id_switch_new'] . "'");
                $data['lastinfo'] = $update_info;
            }
        } else if (isset($_POST['id_switch_old'])) {

            //$sqlold = "SELECT serial_onoff FROM devices WHERE id = '".$_POST['id_switch_old']."'";
            //$resold = query($db,$sqlold);
            //$old = $resold->fetchArray(1);
            $fd = dio_open('/dev/ttyS1', O_RDWR | O_NOCTTY | O_NONBLOCK);
            $sent = explode(".", $_POST['id_switch_old']);
            dio_write($fd, pack("c", "84"));
            dio_write($fd, pack("c", "7"));
            foreach ($sent as $value) {
                dio_write($fd, pack("c", $value));
            }
            dio_close($fd);

        } else if (isset($_POST['update_lastinfo'])) {
            include_once "src/device/devices_new.php";

            $sqlDevices = "SELECT id, lastinfo, model, protocol FROM devices WHERE protocol = 'new'";
            $resDev = query($db, $sqlDevices);

            $devices = array();
            while ($device = $resDev->fetchArray(1)) {
                $type = 'switch';
                foreach ($devices_new as $device_new)
                    if ($device_new['model'] == $device['model']) {
                        $type = $device_new['type'];
                        break;
                    }

                $devices[] = array('id' => $device['id'], 'type' => $type, 'lastinfo' => $device['lastinfo']);
            }
            $data = array('devices' => $devices);
        }
    }
}
//}

header('Content-Type: application/json');
echo json_encode($data);
$db->close();


