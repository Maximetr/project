<?php


class Settings
{
    public static function getSettings($db)
    {
        $sql = "SELECT * FROM settings";

        $result = query($db, $sql);
        if ($result) {
            return $result->fetchArray(1);
        } else {
            return false;
        }
    }

    public static function setNetworkMode($db, $mode)
    {
        if ($mode == 'van') {
            $sql = "UPDATE settings SET network = '".$mode."'";
            if (query($db, $sql)) {
                return true;
            }
        }
        if ($mode == 'lan') {
            $sql = "UPDATE settings SET network = '".$mode."'";
            if (query($db, $sql)) {
                return true;
            }
        }

        return false;
    }

    public static function saveWiFiData ($db, $networkData)
    {
        $sql = "UPDATE settings SET ssid = '".$networkData['ssid']."', encryption = '".$networkData['encryption']."', bssid = '".$networkData['macAdress']."', key = '".$networkData['password']."' WHERE id = '1'";
        if (query($db,$sql)) {
            return true;
        }
        return false;
    }
}